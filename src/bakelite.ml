open Bos
open Astring

let ( let* ) = Result.bind

(** Low-level file accesses *)

let get f =
  (* FIXME: this errors with "unexpected end of file"
   * We use a workaround, but it shouldn't be needed.
     OS.File.read f >>= fun s ->
  *)
  let* s = OS.Cmd.(in_null |> run_io Cmd.(v "cat" % Fpath.to_string f) |> to_string) in
  match int_of_string s with
  | v -> Ok v
  | exception Invalid_argument _ -> Error (`Msg "unparsable brightness value")
let set f brightness =
  OS.Cmd.(in_null |> run_io Cmd.(v "echo" % string_of_int brightness) |> to_file f)


(** Brightness raw and derived values *)

let min_brightness = 1
let max_brightness = lazy (get (Fpath.(Conf.base_dir / "max_brightness")))
let current_brightness = lazy (get (Fpath.(Conf.base_dir / "brightness")))
let set_current_brightness v = set (Fpath.(Conf.base_dir / "brightness")) v
let pct_of_brightness v =
  let* maxb = Lazy.force max_brightness in
  Ok ((v * 100) / maxb)
let brightness_of_pct v =
  let* maxb = Lazy.force max_brightness in
  Ok ((v * maxb) / 100)


(** Sub-commands with parsing *)

type command =
  | Set of int
  | Increase of int
  | Decrease of int

let pct_of_string s =
  match int_of_string s with
  | i when 0 <= i && i <= 100 -> Ok i
  | _ -> Error (`Msg "percentage out of range")
  | exception Failure _ -> Error (`Msg "unparsable percentage")

let parse s =
  if String.length s >= 1 then
    match String.get s 0 with
    | '+' ->
      let* pct = pct_of_string String.Sub.(to_string (v ~start:1 s)) in
      Ok (Increase pct)
    | '-' ->
      let* pct = pct_of_string String.Sub.(to_string (v ~start:1 s)) in
      Ok (Decrease pct)
    | _ ->
      let* pct = pct_of_string s in
      Ok (Set pct)
  else
    Error (`Msg "unparsable command")


(** Error management and main *)

let usage = "bakelite [ PCT | +PCT | -PCT ]"

let main () =
  match Sys.argv with
  | [| |] -> assert false
  | [| _ |] ->
      let* brightness = Lazy.force current_brightness in
      let* pct = pct_of_brightness brightness in
      Format.printf "%d%%\n" pct;
      Ok ()
  | [| _; "-h" |] | [| _; "--help" |] ->
      Format.eprintf "%s\n" usage;
      Ok ()
  | [| _; c |] -> begin
      let* command = parse c in
      match command with
      | Increase inc ->
        let* curr = Lazy.force current_brightness in
        let* curr_pct = pct_of_brightness curr in
        let* brightness = brightness_of_pct (min 100 (curr_pct + inc)) in
        set_current_brightness brightness
      | Decrease dec ->
        let* curr = Lazy.force current_brightness in
        let* curr_pct = pct_of_brightness curr in
        let* brightness = brightness_of_pct (max 1 (curr_pct - dec)) in
        set_current_brightness brightness
      | Set pct ->
        let pct = max pct 1 in
        let* brightness = brightness_of_pct pct in
        set_current_brightness brightness
  end
  | _ ->
    Error `Usage

let () =
  match main () with
  | Ok () -> exit 0
  | Error `Usage ->
      Format.eprintf "%s\n" usage;
      exit 1
  | Error (`Msg msg) ->
      Format.eprintf "Error: %s\n" msg;
      exit 1
