bakelite — changing the screen backlight level
-------------------------------------------------------------------------------

bakelite is a program to check or change the screen backlight level.

## BUILD

### Dependencies

Binaries:
`make`,
`ocamlfind`,
`ocamlopt`.

Libraries (all available in opam):
`bos` and dependencies (`astring`, `rresult`, `fpath`).


### Config (optional)

```
cp src/conf.def.ml src/conf.ml
$EDITOR src/conf.ml
```

### Build

`make`


## PACKAGING

In archlinux, `makepkg` creates a pacman package.


## USAGE

`bakelite`: prints current brightness percentage  
`bakelite PCT`: change the screen brightness to `PCT`%  
`bakelite +PCT`: increase the screen brightness by `PCT`% points  
`bakelite -PCT`: decrease the screen brightness by `PCT`% points  
`bakelite [-h | --help]`: shows usage
