
(** The directory your backlight control is located at. Check the content of
 * the `/sys/class/backlight/` directory if you are not sure. *)
let base_dir = Fpath.v "/sys/class/backlight/acpi_video0/"
